/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.network.clientserverbasics;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;

/**
 *
 * @author lendle
 */
public class DictClient {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        Socket socket = new Socket("dict.org", 2628);
        OutputStream out = socket.getOutputStream();
        //ouputstream適用byte為單位, 不想用outputstream就用writer
        //字會有編碼問題, byte不會
        Writer writer = new OutputStreamWriter(out, "UTF-8");
        //c語言裡, string跟字元陣列是相同東西, 但java不是
        // \r equals enter
        writer.write("DEFINE wn silver\r\n");
        //輸出資料有緩衝區, writer可能會在裡面, flush就是沖馬桶:)
        writer.flush();
        
        InputStream in = socket.getInputStream();
        StringBuilder sb=new StringBuilder();
        //reader底下只能接reader, 所以要有過度類別
        InputStreamReader inputStreamReader=new InputStreamReader(in, "utf-8");
        //bufferedReader一次可讀一個line
        BufferedReader reader=new BufferedReader(inputStreamReader);
        while (true) {            
            String str=reader.readLine();
            //==是記憶體位置
            if(".".equals(str)){
                break;
            }
            sb.append(str);
            sb.append("\r\n");
        }
        
        /*
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(in, "UTF-8"));
        for (String line = reader.readLine(); !line.equals("."); line = reader.readLine()) {
            System.out.println(line);
        }
        */
        System.out.println(sb);
        writer.write("quit\r\n");
        writer.flush();
        socket.close();
    }

}
