/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.network.clientserverbasics;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 *
 * @author lendle
 */
public class TimeClient {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        //hint: 建立 Socket 物件，並且取得 input stream 儲存在 input 變數中
        Socket socket=null;
        InputStream input=null;
        //主機名稱, portnumber, 然後建立連線
        //建立後會佔住資源, 完成後要close
        socket=new Socket("time.nist.gov", 13);
        //將輸入串流抓出
        input=socket.getInputStream();
        /////////////////////////////////////////////////////////////
        //stringBuilder 將char 等等組成string
        StringBuilder builder=new StringBuilder();
        /*1006
        InputStreamReader reader=new InputStreamReader(input);
        for(int c=reader.read(); c!=-1; c=reader.read()){
            builder.append((char)c);
        }
        */
        while(true){
            //從inputstream讀取一個byte
            int b=input.read();
            //b==-1代表沒東西可讀
            if(b==-1){
                break;
            }
            //強制轉型
            char c=(char)b;
            builder.append(c);
        }
        System.out.println(builder);
        socket.close();
    }
    
}
